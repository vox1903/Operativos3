/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package localfilesystem;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author juanguerrero
 */
public class Server {
    
    private ArrayList<Client> clients = new ArrayList<Client>();
    
    public void createDisk(String clientName,int totalSectors,int sectorSize,String rootName){
        Client client = searchClient(clientName);
        client.createDisk(totalSectors,sectorSize,rootName);
    }
    
    public void createDirectory(String clientName,String name){
        Client client = searchClient(clientName);
        client.createDirectory(name);
    }
    
    public boolean createFile(String clientName,String name,String extension,String content) throws IOException{
        Client client = searchClient(clientName);
        return client.createFile(name,extension,content);
    }
    
    public void moveDirectory(String clientName,String path){
        Client client = searchClient(clientName);
        client.moveDirectory(path);
    }
    
    public String listFiles(String clientName){
        Client client = searchClient(clientName);
        return client.listFiles();
    }
    
    void changeFileContent(String clientName,String filename,String content) throws IOException{
        Client client = searchClient(clientName);
        client.changeFileContent(filename,content);
    }
    
    public String fileProperties(String clientName,String filename){
        Client client = searchClient(clientName);
        return client.fileProperties(filename);
    }
    
    public String fileContent(String clientName,String filename) throws IOException{
        Client client = searchClient(clientName);
        return client.fileContent(filename);
    }
    
    public String moveFile(String clientName,String filename,String path) throws IOException{
        Client client = searchClient(clientName);
        return client.fileContent(filename);
    }
    
    public void removeFile(String clientName,String filename) throws IOException{
        Client client = searchClient(clientName);
        client.removeFile(filename);
    }
    
    public String getPathTree(String clientName){
        Client client = searchClient(clientName);
        return client.getPathTree();
    }
    
    private Client searchClient(String name){
        for (Client client : clients){
            if (client.getName().equals(name)){
                return client;
            }
        }
        return null;
    }
    
    public void readFile(String clientName, String path) throws IOException{
        Client client = searchClient(clientName);
        client.readFile(path);
    }
    
    public void writeFile(String clientName,String path, String fileName) throws IOException{
        Client client = searchClient(clientName);
        client.writeFile(path, fileName);
    }
    
    public void addClient(String name){
        clients.add(new Client(name));
    }
    
    public String getPath(String clientName){
        Client client = searchClient(clientName);
        return client.getPath();
    }
    
    public boolean checkFile(String clientName,String fileName){
        Client client = searchClient(clientName);
        return client.checkFile(fileName);
    }
    
    
}
