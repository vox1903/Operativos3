/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package localfilesystem;

import java.io.IOException;

/**
 *
 * @author juanguerrero
 */
public class LocalFileSystem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Server server = new Server();
        server.addClient("Me");
        server.createDisk("Me",10, 20,"root");
        server.createDirectory("Me", "Dir1");
        server.createDirectory("Me", "Dir2");
        server.createDirectory("Me", "Dir3");
        
        //server.createFile("Me", "Helloworld", "txt", "Hello World!asdasdasdasdasdasdas");
        server.createFile("Me", "Glorg", "txt", "Tres tristes tigres trigo tragaban en un trigal");
        server.createFile("Me", "Glorg2", "txt", "Tres tristes tigres trigo tragaban en un trigal");
        server.createFile("Me", "Glorg3", "txt", "Tres tristes tigres trigo tragaban en un trigal");
        server.createFile("Me", "Glorg4", "txt", "Tres tristes tigres trigo tragaban ");
        
//        System.out.println(server.listFiles("Me"));
//        System.out.println("======");
//        
//        server.moveDirectory("Me","Dir1");
//        server.createFile("Me", "Glorg", "txt", "222");
//        server.createFile("Me", "Glorg2", "txt", "333");
////        
//        server.moveDirectory("Me","..");
//        System.out.println(server.listFiles("Me"));
//        System.out.println("======");

//        server.removeFile("Me", "Glorg2");
//        
        server.createFile("Me", "Glorg5", "txt", "EASY LOVER");
        server.createFile("Me", "Glorg6", "txt", "EASY LOVER");

        //System.out.println(server.getPathTree("Me"));

        System.out.println("======");
        System.out.println(server.getPath("Me"));
        server.moveDirectory("Me","Dir1");
        System.out.println("======");
        System.out.println(server.getPath("Me"));
        server.moveDirectory("Me","..");
        System.out.println("======");
        System.out.println(server.getPath("Me"));






//        
//        
//        server.addClient("Me2");
//        server.createDisk("Me2",10, 20,"root2");
//        server.createDirectory("Me2", "Dir1");
//        server.createDirectory("Me2", "Dir2");
//        server.createDirectory("Me2", "Dir3");
//        
//        //server.createFile("Me", "Helloworld", "txt", "Hello World!asdasdasdasdasdasdas");
//        server.createFile("Me2", "Glorg", "txt", "Tres tristes tigres trigo tragaban en un trigal");
//        server.createFile("Me2", "Glorg2", "txt", "Tres tristes tigres trigo tragaban en un trigal");
//        server.createFile("Me2", "Glorg3", "txt", "Tres tristes tigres trigo tragaban en un trigal");
//        server.createFile("Me2", "Glorg4", "txt", "Tres tristes tigres trigo tragaban ");
//        
////        System.out.println(server.listFiles("Me"));
////        System.out.println("======");
////        
//        server.moveDirectory("Me2","Dir1");
//        server.createFile("Me2", "Glorg", "txt", "222");
//        server.createFile("Me2", "Glorg2", "txt", "333");
////        
//        server.moveDirectory("Me2","..");
////        System.out.println(server.listFiles("Me"));
////        System.out.println("======");
//
//        server.removeFile("Me2", "Glorg2");
//        
//        server.createFile("Me2", "Glorg5", "txt", "EASY LOVER");
//        server.createFile("Me2", "Glorg6", "txt", "EASY LOVER");
//
//        System.out.println(server.getPathTree("Me2"));
//        
//        
//        //server.createFile("Me", "Glorg5", "txt", "Tres tristes tigres trigo tragaban en un trigal");
//        //server.createFile("Me", "Bob", "xls", "Banana");
//        
////        System.out.println("======");
////        System.out.println(server.fileProperties("Me", "Glorg"));
////        System.out.println("======");
////        System.out.println(server.fileContent("Me", "Glorg"));
////        System.out.println("======");
//        
//        //System.out.println(server.listFiles("Me"));
//        //System.out.println(server.getPathTree("Me"));
        
    }
    
}
