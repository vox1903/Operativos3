/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesystemclient;
import FileSystemWS.FileSystemWS_Service;
import FileSystemWS.FileSystemWS;
/**
 *
 * @author jostin
 */
public class FileSystemClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FileSystemWS_Service service = new FileSystemWS_Service();
        final FileSystemWS client = service.getFileSystemWSPort();
        System.out.println(client.hello("Juan"));
        System.out.println(client.square(2));
        
    }
    
}
