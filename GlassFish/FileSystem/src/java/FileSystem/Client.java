/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileSystem;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author juanguerrero
 */
public class Client {
    
    final private String name;
    private String rootName;
    private Node<File> fileSystemBase;
    private Node<File> currentPath;
    String localPath;
    
    public Client(String pName){
        name = pName;
        localPath = "";
    }
    
    public void createDisk(int totalSectors,int sectorSize,String root){
        this.rootName = root + ".txt";
        try{
        PrintWriter writer = new PrintWriter("C:\\Users\\josti\\Documents\\" + this.rootName, "UTF-8");
            for (int i = 0; i < totalSectors;i ++){
                for (int j = 0; j < sectorSize; j ++){
                    writer.print("0");
                }
                if (i + 1 != totalSectors ){writer.println("");}
            }
            writer.close();
        } catch (IOException e) {
           System.out.println("Error on createDisk()");
        }
        fileSystemBase = new Node<File>(new Directory(root));
        currentPath = fileSystemBase;
        localPath = "/" + root;
    }
    
    private Node<File> searchFile( String fileName){
        List<Node<File>> childs = currentPath.getChildren();
        for (Node<File> child : childs){
            if (child.getData().getName().equals(fileName)){
                return child;
            }
        }
        return null;
    }
    
    private String readDiskContent(File file) throws IOException{
        String content = "";
        String line;
        int sector = file.getSector();
        int start = file.getStart();
        int fileSize = file.getSize();
        int end;
        
        try (
            InputStream fis = new FileInputStream("C:\\Users\\josti\\Documents\\" + this.rootName);
            InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
            BufferedReader br = new BufferedReader(isr);
        ){
            //SKIP ALL CONTENT TILL THE RIGHT SECTOR
            for (int i = 0;i < sector; i ++){
                br.readLine();
            }
            //READ THE CONTENT
            while (fileSize > 0){
                line = br.readLine();
                if (fileSize > line.length()){
                    end = line.length();
                    content += line.substring(start);
                }else{
                    end = fileSize;
                    content += line.substring(start, end);
                }
                fileSize -= end - start;
                start = 0;
            }
        }
        
        return content;
    }
    
    private boolean storeDiskFile(File file, String content) throws FileNotFoundException, IOException{
        String line;
        int end,space,bestSpace,startBestSpace,startSpace,sectorSpace,sectorBestSpace;
        end = space = bestSpace = startBestSpace = startSpace = sectorSpace =sectorBestSpace = -1;
        int fileSpace = file.getSize();
        boolean freeSpace = false;
        PrintWriter writer;
        
        try (
            InputStream fis = new FileInputStream("C:\\Users\\josti\\Documents\\" + this.rootName);
            InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
            BufferedReader br = new BufferedReader(isr);
        ){
            ArrayList<String> fileContent;
            fileContent = new ArrayList<String>();
            while ((line = br.readLine()) != null){
                fileContent.add(line);
            }

            for (int i = 0; i < fileContent.size();i ++){
                line = fileContent.get(i);
                for (int j = 0; j < line.length(); j ++){
                    if (line.charAt(j) == '0'){
                        if (!freeSpace){startSpace = j; sectorSpace = i; space = 0; }
                        space ++;
                        freeSpace = true;
                    }else{
                        if (freeSpace){
                            if ((space - fileSpace < bestSpace || bestSpace == -1) && space >= fileSpace){
                                bestSpace = space;
                                startBestSpace = startSpace;
                                sectorBestSpace = sectorSpace;
                            }
                        }
                        freeSpace = false;
                        break;
                    }
                }
                if (i + 1 == fileContent.size()){
                    if (freeSpace){
                        if ((space - fileSpace < bestSpace || bestSpace == -1) && space >= fileSpace){
                            bestSpace = space;
                            startBestSpace = startSpace;
                            sectorBestSpace = sectorSpace;
                        }
                    }
                }
            }
            
            if (bestSpace == -1){
                return false;
            }
            
            file.setStart(startBestSpace);
            file.setSector(sectorBestSpace);
            //space = file.getSize();
            for (int i = 0;i < fileSpace;i ++){
                line = fileContent.get(sectorBestSpace);
                char[] chars = line.toCharArray();
                chars[startBestSpace] = content.charAt(i);
                line = String.valueOf(chars);
                fileContent.set(sectorBestSpace, line);
                
                startBestSpace ++;
                
                if (startBestSpace == line.length()){
                    sectorBestSpace ++;
                    startBestSpace = 0;
                }
            }
            
            writer = new PrintWriter("C:\\Users\\josti\\Documents\\" + this.rootName, "UTF-8");
            for (String current : fileContent){
                writer.println(current);
            }
            writer.close();
        }
        return true;
    }
    
    private void storeDiskNewContent(File file, String content){
        
    }
    
    private void removeDiskFile(Node<File> node) throws IOException{
        if (node.isRoot()){
            return;
        }
        removeDiskFileHelper(node);
        Node<File> parent = node.getParent();
        parent.removeChildren(node);
    }
    
    
    private void removeFile(File file) throws IOException{
        String line;
        int fileSpace = file.getSize();
        int sector = file.getSector();
        int start = file.getStart();
        PrintWriter writer;
        
        try (
            InputStream fis = new FileInputStream("C:\\Users\\josti\\Documents\\" + this.rootName);
            InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
            BufferedReader br = new BufferedReader(isr);
        ){
            ArrayList<String> fileContent;
            fileContent = new ArrayList<String>();
            while ((line = br.readLine()) != null){
                fileContent.add(line);
            }

            for (int i = 0;i < fileSpace;i ++){
                line = fileContent.get(sector);
                char[] chars = line.toCharArray();
                chars[start] = '0';
                line = String.valueOf(chars);
                fileContent.set(sector, line);
                
                start ++;
                
                if (start == line.length()){
                    sector ++;
                    start = 0;
                }
            }
            
            writer = new PrintWriter("C:\\Users\\josti\\Documents\\" + this.rootName, "UTF-8");
            for (String current : fileContent){
                writer.println(current);
            }
            writer.close();
        }
    }
    
    private void removeDiskFileHelper(Node<File> current) throws IOException{
        if (current.getData() instanceof ConcreteFile){
            removeFile(current.getData());
        }else{
            for (Node<File> child : current.getChildren()){
                removeDiskFileHelper(child);
            }
        }
    }
    
    public void createDirectory(String pName){
        Node<File> checkFile = searchFile(name);
        if(checkFile != null){
            return;
        }
        currentPath.addChild(new Directory(pName));
    }
    
    public boolean createFile(String name,String extension,String content) throws IOException{
        Node<File> checkFile = searchFile(name);
        if(checkFile != null){
            return false;
        }
        File newFile = new ConcreteFile(name);
        newFile.setSize(content.length());
        newFile.setExtension(extension);
        boolean check = storeDiskFile(newFile,content);
        
        if (check){
            currentPath.addChild(newFile);
            return true;
        }
        return false;
    }
    
    public void moveDirectory(String fileName){
        if (fileName.equals("..")){
            Node<File> parent = currentPath.getParent();
            if (parent != null){
                currentPath = parent;
                int endIndex = localPath.lastIndexOf("/");
                localPath = localPath.substring(0, endIndex);
            }
            return;
        }
        
        Node<File> dir = searchFile(fileName);
        if (dir != null && dir.getData() instanceof Directory){
            currentPath = dir;
            localPath += "/" + dir.getData().getName();
        }
    }
    
    public String getPathTree(){
        String result = "";
        int level = 0;
        return getPathTreeHelper(currentPath,result,level);
    }
    
    
    public String getPathTreeHelper(Node<File> current,String result, int level){
        for (int i = 0; i < level; i++) {
            result += "\t";
        }
        
        if (current.getData() instanceof ConcreteFile){
            result += current.getData().getName() + "." + current.getData().getExtension();
            return result;
        }else{
            result += current.getData().getName();
        }
        
        for (Node<File> child : current.getChildren()){
            result += "\n";
            result = getPathTreeHelper(child,result,level + 1);
        }
        
        return result;
    }
    
    public void changeFileContent(String fileName,String content) throws IOException{
        Node<File> file = searchFile(fileName);
        if (file != null && file.getData() instanceof ConcreteFile){
            String name = file.getData().getName();
            String extension = file.getData().getExtension();
            String oldContent = this.fileContent(fileName);
            
            this.removeDiskFile(file);
            boolean answer = this.createFile(fileName, extension, content);
            if(!answer){
                this.createFile(fileName, extension, oldContent);
            }
        }
    }
    
    public String fileProperties(String fileName){    
        String result = "";
        Node<File> file = searchFile(fileName);
        if (file == null){return "";}
       
        result += "File name : " + file.getData().getName() + "\n";
        
        if (file.getData() instanceof ConcreteFile){
            result += "Extension : " + file.getData().getExtension() + "\n";
        }
        
        result += "Creation date : " + file.getData().getCreationDate().toString() + "\n";
        result += "Last modification date : " + file.getData().getModificationDate().toString() + "\n";
        
        if (file.getData() instanceof ConcreteFile){
            result += "Size : " + String.valueOf(file.getData().getSize());
        }
        
        return result;
    }
    
    public String fileContent(String fileName) throws IOException{
        String result = "";
        Node<File> file = searchFile(fileName);
        if (file == null){return "";}
        
        if (file.getData() instanceof ConcreteFile){
            result += "Content : ";
            result += readDiskContent(file.getData());
        }
        
        return result;
    }
    
    public void moveFile(String filename,String path){
        
    }
    
    public boolean checkFile(String fileName){
        Node<File> file = searchFile(fileName);
        if (file != null){
            return true;
        }
        return false;
    }
    
    public void removeFile(String fileName) throws IOException{
        Node<File> file = searchFile(fileName);
        removeDiskFile(file);
    }
    
    public String listFiles(){
        String result = "";
        
        for (Node<File> child : currentPath.getChildren()){
            if (child.getData() instanceof ConcreteFile){
                result += child.getData().getName() + "." + child.getData().getExtension() + "\n";
            }else{
                result += child.getData().getName() + "\n";
            }
        }
        
        return result;
    }
    
    public String getPath(){
        return localPath;
    }
    

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    
    public void readFile(String path) throws IOException{
       //check SO
       String so = System.getProperty("os.name"); 
       String separator = "/"; 
       if(so.toLowerCase().contains("win")){
           separator = "\\";   
       }
       
       System.out.println("SO: " + so);
        
       String newpath = path.replace(separator, separator+separator);
       
       int indName = path.lastIndexOf(separator);
       int indExt = path.lastIndexOf('.');
       
       String fileName = (String) path.subSequence(indName+1, indExt);
       String extension = (String) path.subSequence(indExt+1, path.length());
       
        try{
            String content = new String(Files.readAllBytes(Paths.get(newpath)), StandardCharsets.UTF_8);
            this.createFile(fileName, extension, content);
        }
        catch(NoSuchFileException e){
            System.out.println("Invalid file");
        }
        
    }
    
    public void writeFile(String path, String fileName) throws IOException{
        Node<File> file = searchFile(fileName);
        if (file != null && file.getData() instanceof ConcreteFile){
            String extension = file.getData().getExtension();
            String content = this.readDiskContent(file.getData());
            
            writeFileHelper(path, fileName, extension, content);
        }
        
    }
    
    
    public void writeFileHelper(String path, String filename, String ext, String content) throws IOException{
        //check SO
        String so = System.getProperty("os.name"); 
        String separator = "/"; 
        if(so.toLowerCase().contains("win")){
            separator = "\\";   
        }

        String newpath = path.replace(separator, separator+separator);
        
        if(Files.exists(Paths.get(newpath))) {
            return;
        }
        java.io.File fout = new java.io.File(path);
        FileOutputStream fos = new FileOutputStream(fout); 
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        bw.write(content);
        bw.close();
        System.out.println("File created"); 
    }
    
    
}
