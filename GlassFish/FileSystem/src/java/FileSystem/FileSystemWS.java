/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileSystem;

import java.io.IOException;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author jostin
 */
@WebService(serviceName = "FileSystemWS")
public class FileSystemWS {
    Server server = new Server();
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }
    
    @WebMethod(operationName = "addClient")
    public String addClient(String client) {
        server.addClient(client);
        return "Client Added";
    }
     
    @WebMethod(operationName = "createDisk")
    public String createDisk(String client, List<String> commands) {
        if(commands.size() == 4){      
            try{
                int sectors = Integer.parseInt(commands.get(1));
                int size = Integer.parseInt(commands.get(2));
                String rootName = commands.get(3);
                server.createDisk(client, sectors, size, rootName);
                return "Created Disk";
            }
            catch(NumberFormatException e){
                return "Error on Parameters";
            }
        }
        else{
             return "Wrong number of parameters";
        }   
    }
    
    @WebMethod(operationName = "createDirectory")
    public String createDirectory(String client, List<String> commands) {
        if(commands.size() == 2){
            String dirName = commands.get(1);
            server.createDirectory(client, dirName);
            return "Directory created";
        }
        else{
            return "Wrong number of parameters";
        }   
     
    }
    
    @WebMethod(operationName = "createFile")
    public void createFile(String client, String fileName, String ext, String content) throws IOException {
        server.createFile(client, fileName, ext, content); 
    }
    
    @WebMethod(operationName = "listDir")
    public String listDir(String client) {
        return server.listFiles(client);  
    }
    
    @WebMethod(operationName = "moveDir")
    public String moveDir(String client, List<String> commands) {
         if(commands.size() == 2){
            String newDir = commands.get(1);
            server.moveDirectory(client, newDir);
            return ""; 
        }
        else{
            return "Wrong number of parameters";
        }  
         
    }
    
    @WebMethod(operationName = "fileProperties")
    public String fileProperties(String client, List<String> commands) {
        if(commands.size() == 2){
            String fileName = commands.get(1);
            return server.fileProperties(client, fileName);
            
        }
        else{
            return "Wrong number of parameters";
        }    
        
    }
    
    @WebMethod(operationName = "fileContent")
    public String fileContent(String client, List<String> commands) throws IOException {
        if(commands.size() == 2){
            String fileName = commands.get(1);
            return server.fileContent(client, fileName);
            
        }
        else{
            return "Wrong number of parameters";
        }      
    }
    
    @WebMethod(operationName = "changeFile")
    public void changeFile(String client, String fileName, String content) throws IOException {
        server.changeFileContent(client, fileName, content);
    }
    
    @WebMethod(operationName = "removeFile")
    public String removeFile(String client, List<String> commands) throws IOException {
        String fileName = commands.get(1);
        server.removeFile(client, fileName);
        return (fileName + " removed");      
    }
    
    @WebMethod(operationName = "getCurPath")
    public String getCurrentPath(String client) throws IOException {
        return server.getPath(client);
    }
    
    @WebMethod(operationName = "getTree")
    public String getTree(String client) throws IOException {
        return server.getPathTree(client);
    }
    
    @WebMethod(operationName = "copyFile")
    public String copyFile(String client, List<String> commands) throws IOException {
        if(commands.size() == 3 || commands.size() == 4 ){
            String type = commands.get(1);
            String path = commands.get(2);
            String filename = "";
            if(commands.size() == 4){
                filename = commands.get(3);
            }
            
            switch(type){
                case "-rv":
                    server.readFile(client, path);
                    break;
                case "-vr":
                    server.writeFile(client, path, filename);
                    break;
                case "-vv":
                    //call function
                    break;
                default:
                    return "Invalid type";
            }
        }
        else{
            return "Wrong number of parameters";
        } 
        return "";
    }
    
    @WebMethod(operationName = "movFile")
    public String moveFile(String client, List<String> commands) {
        if(commands.size() == 3){
            String fileName = commands.get(1);
            String path = commands.get(2);
            //return server.moveFile(client, fileName, path);
        }
        else{
            return "Wrong number of parameters";
        }
        return "";
    }
    
    @WebMethod(operationName = "checkDir")
    public boolean checkDir(String client, String file ) {
        return server.checkFile(client, file);
    }
    
}