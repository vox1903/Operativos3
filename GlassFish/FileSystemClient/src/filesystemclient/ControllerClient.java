/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesystemclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;


/**
 *
 * @author jostin
 */
public class ControllerClient {
    ArrayList<User> userList;

    public ControllerClient() {
        userList = new ArrayList<>();
    }
    
    public void addUser(User pUser){
        userList.add(pUser);
    }
    
    public boolean checkUser(String pUsername, String pPassword){
        for(User user : userList){
            if(user.getUsername().equals(pUsername) && user.getPassword().equals(pPassword)){
                return true;
            }
        }
       return false;
    } 
    
    public void crtFunction(String [] commands){
        if(commands.length == 4){
            
            try{
                int sectors = Integer.parseInt(commands[1]);
                int size = Integer.parseInt(commands[2]);
                String rootName = commands[3];
                //call function
            }
            catch(NumberFormatException e){
                System.out.println("Error on Parameters");
            }
        }
        else{
            System.out.println("Wrong number of parameters");
        }    
    }
    
    public void fleFunction(String [] commands){
        if(commands.length == 3){
            String fileName = commands[1];
            String content = commands[2];
            //call function
        }
        else{
            System.out.println("Wrong number of parameters");
        }    
    }
    
    public void mkdirFunction(String [] commands){
        if(commands.length == 2){
            String dirName = commands[1];
            //call function
        }
        else{
            System.out.println("Wrong number of parameters");
        }    
    }
    
    public void chdirFunction(String [] commands){
        if(commands.length == 2){
            String newDir = commands[1];
            //call function
        }
        else{
            System.out.println("Wrong number of parameters");
        }    
    }
    
     public void ldirFunction(){
        //call function
        
            
    }
    
    public void mfleFunction(String [] commands){
        if(commands.length == 3){
            String fileName = commands[1];
            String newContent = commands[2];
            //call function
        }
        else{
            System.out.println("Wrong number of parameters");
        }    
    }
    
    public void pptFunction(String [] commands){
        if(commands.length == 2){
            String fileName = commands[1];
            //call function
        }
        else{
            System.out.println("Wrong number of parameters");
        }    
    }
    
    public void viewFunction(String [] commands){
        if(commands.length == 2){
            String fileName = commands[1];
            //call function
        }
        else{
            System.out.println("Wrong number of parameters");
        }    
    }
    
    public void cpyFunction(String [] commands) throws IOException{
        if(commands.length == 4){
            String type = commands[1];
            String path = commands[2];
            String newPath = commands[3];
            switch(type){
                case "-rv":
                    //call function
                    readFile(path);
                    break;
                case "-vr":
                    writeFile(newPath, "", "" , "Hola Mundo");
                    //call function
                    break;
                case "-vv":
                    //call function
                    break;
                default:
                    System.out.println("Invalid type");
            }
        }
        else{
            System.out.println("Wrong number of parameters");
        }    
    }
    
    public void movFunction(String [] commands){
        if(commands.length == 3){
            String fileName = commands[1];
            String path = commands[2];
            //call function
        }
        else{
            System.out.println("Wrong number of parameters");
        }    
    }
    
    public void remFunction(String [] commands){
        //call function
    }
    
    public void treeFunction(){
        //call function
    }
    
    public void readFile(String path) throws IOException{
       //check SO
       String so = System.getProperty("os.name"); 
       String separator = "/"; 
       if(so.toLowerCase().contains("win")){
           separator = "\\";   
       }
       
       System.out.println("SO: " + so);
        
       String newpath = path.replace(separator, separator+separator);
       
       int indName = path.lastIndexOf(separator);
       int indExt = path.lastIndexOf('.');
       
       String fileName = (String) path.subSequence(indName+1, indExt);
       String extension = (String) path.subSequence(indExt, path.length());
       
        try{
            String content = new String(Files.readAllBytes(Paths.get(newpath)), StandardCharsets.UTF_8);
            System.out.println("Filename: " + fileName);
            System.out.println("Ext: " + extension);
            System.out.println(content);
        }
        catch(NoSuchFileException e){
            System.out.println("Invalid file");
        }
        
    }
    
    public void writeFile(String path, String filename, String ext, String content) throws IOException{
        //check SO
        String so = System.getProperty("os.name"); 
        String separator = "/"; 
        if(so.toLowerCase().contains("win")){
            separator = "\\";   
        }

        String newpath = path.replace(separator, separator+separator);
        
        BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
        
        if(Files.exists(Paths.get(newpath))) { 
            System.out.print("File already exists, replace this file? (y/n): ");
            String answer = buffer.readLine();
            if(answer.equals("n")){
                return;
            }    
        }
        
        File fout = new File(path);
	FileOutputStream fos = new FileOutputStream(fout); 
	BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        bw.write(content);
        bw.close();
        System.out.println("File created"); 
    }
    
}
