
package FileSystemWS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the FileSystemWS package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RemoveFile_QNAME = new QName("http://FileSystem/", "removeFile");
    private final static QName _ListDir_QNAME = new QName("http://FileSystem/", "listDir");
    private final static QName _FileContentResponse_QNAME = new QName("http://FileSystem/", "fileContentResponse");
    private final static QName _GetTree_QNAME = new QName("http://FileSystem/", "getTree");
    private final static QName _RemoveFileResponse_QNAME = new QName("http://FileSystem/", "removeFileResponse");
    private final static QName _CopyFileResponse_QNAME = new QName("http://FileSystem/", "copyFileResponse");
    private final static QName _CreateDirectory_QNAME = new QName("http://FileSystem/", "createDirectory");
    private final static QName _FilePropertiesResponse_QNAME = new QName("http://FileSystem/", "filePropertiesResponse");
    private final static QName _AddClient_QNAME = new QName("http://FileSystem/", "addClient");
    private final static QName _CopyFile_QNAME = new QName("http://FileSystem/", "copyFile");
    private final static QName _CreateDiskResponse_QNAME = new QName("http://FileSystem/", "createDiskResponse");
    private final static QName _CreateFile_QNAME = new QName("http://FileSystem/", "createFile");
    private final static QName _AddClientResponse_QNAME = new QName("http://FileSystem/", "addClientResponse");
    private final static QName _CreateDirectoryResponse_QNAME = new QName("http://FileSystem/", "createDirectoryResponse");
    private final static QName _HelloResponse_QNAME = new QName("http://FileSystem/", "helloResponse");
    private final static QName _GetCurPathResponse_QNAME = new QName("http://FileSystem/", "getCurPathResponse");
    private final static QName _ListDirResponse_QNAME = new QName("http://FileSystem/", "listDirResponse");
    private final static QName _FileContent_QNAME = new QName("http://FileSystem/", "fileContent");
    private final static QName _CheckDir_QNAME = new QName("http://FileSystem/", "checkDir");
    private final static QName _MovFileResponse_QNAME = new QName("http://FileSystem/", "movFileResponse");
    private final static QName _ChangeFileResponse_QNAME = new QName("http://FileSystem/", "changeFileResponse");
    private final static QName _Hello_QNAME = new QName("http://FileSystem/", "hello");
    private final static QName _CreateFileResponse_QNAME = new QName("http://FileSystem/", "createFileResponse");
    private final static QName _GetCurPath_QNAME = new QName("http://FileSystem/", "getCurPath");
    private final static QName _MoveDirResponse_QNAME = new QName("http://FileSystem/", "moveDirResponse");
    private final static QName _CheckDirResponse_QNAME = new QName("http://FileSystem/", "checkDirResponse");
    private final static QName _GetTreeResponse_QNAME = new QName("http://FileSystem/", "getTreeResponse");
    private final static QName _MovFile_QNAME = new QName("http://FileSystem/", "movFile");
    private final static QName _MoveDir_QNAME = new QName("http://FileSystem/", "moveDir");
    private final static QName _CreateDisk_QNAME = new QName("http://FileSystem/", "createDisk");
    private final static QName _FileProperties_QNAME = new QName("http://FileSystem/", "fileProperties");
    private final static QName _IOException_QNAME = new QName("http://FileSystem/", "IOException");
    private final static QName _ChangeFile_QNAME = new QName("http://FileSystem/", "changeFile");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: FileSystemWS
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CopyFile }
     * 
     */
    public CopyFile createCopyFile() {
        return new CopyFile();
    }

    /**
     * Create an instance of {@link AddClient }
     * 
     */
    public AddClient createAddClient() {
        return new AddClient();
    }

    /**
     * Create an instance of {@link CreateDiskResponse }
     * 
     */
    public CreateDiskResponse createCreateDiskResponse() {
        return new CreateDiskResponse();
    }

    /**
     * Create an instance of {@link CreateFile }
     * 
     */
    public CreateFile createCreateFile() {
        return new CreateFile();
    }

    /**
     * Create an instance of {@link FileContentResponse }
     * 
     */
    public FileContentResponse createFileContentResponse() {
        return new FileContentResponse();
    }

    /**
     * Create an instance of {@link GetTree }
     * 
     */
    public GetTree createGetTree() {
        return new GetTree();
    }

    /**
     * Create an instance of {@link RemoveFileResponse }
     * 
     */
    public RemoveFileResponse createRemoveFileResponse() {
        return new RemoveFileResponse();
    }

    /**
     * Create an instance of {@link RemoveFile }
     * 
     */
    public RemoveFile createRemoveFile() {
        return new RemoveFile();
    }

    /**
     * Create an instance of {@link ListDir }
     * 
     */
    public ListDir createListDir() {
        return new ListDir();
    }

    /**
     * Create an instance of {@link CreateDirectory }
     * 
     */
    public CreateDirectory createCreateDirectory() {
        return new CreateDirectory();
    }

    /**
     * Create an instance of {@link FilePropertiesResponse }
     * 
     */
    public FilePropertiesResponse createFilePropertiesResponse() {
        return new FilePropertiesResponse();
    }

    /**
     * Create an instance of {@link CopyFileResponse }
     * 
     */
    public CopyFileResponse createCopyFileResponse() {
        return new CopyFileResponse();
    }

    /**
     * Create an instance of {@link GetTreeResponse }
     * 
     */
    public GetTreeResponse createGetTreeResponse() {
        return new GetTreeResponse();
    }

    /**
     * Create an instance of {@link MovFile }
     * 
     */
    public MovFile createMovFile() {
        return new MovFile();
    }

    /**
     * Create an instance of {@link MoveDir }
     * 
     */
    public MoveDir createMoveDir() {
        return new MoveDir();
    }

    /**
     * Create an instance of {@link CreateFileResponse }
     * 
     */
    public CreateFileResponse createCreateFileResponse() {
        return new CreateFileResponse();
    }

    /**
     * Create an instance of {@link GetCurPath }
     * 
     */
    public GetCurPath createGetCurPath() {
        return new GetCurPath();
    }

    /**
     * Create an instance of {@link MoveDirResponse }
     * 
     */
    public MoveDirResponse createMoveDirResponse() {
        return new MoveDirResponse();
    }

    /**
     * Create an instance of {@link CheckDirResponse }
     * 
     */
    public CheckDirResponse createCheckDirResponse() {
        return new CheckDirResponse();
    }

    /**
     * Create an instance of {@link FileProperties }
     * 
     */
    public FileProperties createFileProperties() {
        return new FileProperties();
    }

    /**
     * Create an instance of {@link IOException }
     * 
     */
    public IOException createIOException() {
        return new IOException();
    }

    /**
     * Create an instance of {@link ChangeFile }
     * 
     */
    public ChangeFile createChangeFile() {
        return new ChangeFile();
    }

    /**
     * Create an instance of {@link CreateDisk }
     * 
     */
    public CreateDisk createCreateDisk() {
        return new CreateDisk();
    }

    /**
     * Create an instance of {@link GetCurPathResponse }
     * 
     */
    public GetCurPathResponse createGetCurPathResponse() {
        return new GetCurPathResponse();
    }

    /**
     * Create an instance of {@link AddClientResponse }
     * 
     */
    public AddClientResponse createAddClientResponse() {
        return new AddClientResponse();
    }

    /**
     * Create an instance of {@link CreateDirectoryResponse }
     * 
     */
    public CreateDirectoryResponse createCreateDirectoryResponse() {
        return new CreateDirectoryResponse();
    }

    /**
     * Create an instance of {@link HelloResponse }
     * 
     */
    public HelloResponse createHelloResponse() {
        return new HelloResponse();
    }

    /**
     * Create an instance of {@link MovFileResponse }
     * 
     */
    public MovFileResponse createMovFileResponse() {
        return new MovFileResponse();
    }

    /**
     * Create an instance of {@link ChangeFileResponse }
     * 
     */
    public ChangeFileResponse createChangeFileResponse() {
        return new ChangeFileResponse();
    }

    /**
     * Create an instance of {@link Hello }
     * 
     */
    public Hello createHello() {
        return new Hello();
    }

    /**
     * Create an instance of {@link ListDirResponse }
     * 
     */
    public ListDirResponse createListDirResponse() {
        return new ListDirResponse();
    }

    /**
     * Create an instance of {@link FileContent }
     * 
     */
    public FileContent createFileContent() {
        return new FileContent();
    }

    /**
     * Create an instance of {@link CheckDir }
     * 
     */
    public CheckDir createCheckDir() {
        return new CheckDir();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "removeFile")
    public JAXBElement<RemoveFile> createRemoveFile(RemoveFile value) {
        return new JAXBElement<RemoveFile>(_RemoveFile_QNAME, RemoveFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListDir }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "listDir")
    public JAXBElement<ListDir> createListDir(ListDir value) {
        return new JAXBElement<ListDir>(_ListDir_QNAME, ListDir.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileContentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "fileContentResponse")
    public JAXBElement<FileContentResponse> createFileContentResponse(FileContentResponse value) {
        return new JAXBElement<FileContentResponse>(_FileContentResponse_QNAME, FileContentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTree }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "getTree")
    public JAXBElement<GetTree> createGetTree(GetTree value) {
        return new JAXBElement<GetTree>(_GetTree_QNAME, GetTree.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "removeFileResponse")
    public JAXBElement<RemoveFileResponse> createRemoveFileResponse(RemoveFileResponse value) {
        return new JAXBElement<RemoveFileResponse>(_RemoveFileResponse_QNAME, RemoveFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CopyFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "copyFileResponse")
    public JAXBElement<CopyFileResponse> createCopyFileResponse(CopyFileResponse value) {
        return new JAXBElement<CopyFileResponse>(_CopyFileResponse_QNAME, CopyFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDirectory }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "createDirectory")
    public JAXBElement<CreateDirectory> createCreateDirectory(CreateDirectory value) {
        return new JAXBElement<CreateDirectory>(_CreateDirectory_QNAME, CreateDirectory.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FilePropertiesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "filePropertiesResponse")
    public JAXBElement<FilePropertiesResponse> createFilePropertiesResponse(FilePropertiesResponse value) {
        return new JAXBElement<FilePropertiesResponse>(_FilePropertiesResponse_QNAME, FilePropertiesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddClient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "addClient")
    public JAXBElement<AddClient> createAddClient(AddClient value) {
        return new JAXBElement<AddClient>(_AddClient_QNAME, AddClient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CopyFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "copyFile")
    public JAXBElement<CopyFile> createCopyFile(CopyFile value) {
        return new JAXBElement<CopyFile>(_CopyFile_QNAME, CopyFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDiskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "createDiskResponse")
    public JAXBElement<CreateDiskResponse> createCreateDiskResponse(CreateDiskResponse value) {
        return new JAXBElement<CreateDiskResponse>(_CreateDiskResponse_QNAME, CreateDiskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "createFile")
    public JAXBElement<CreateFile> createCreateFile(CreateFile value) {
        return new JAXBElement<CreateFile>(_CreateFile_QNAME, CreateFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddClientResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "addClientResponse")
    public JAXBElement<AddClientResponse> createAddClientResponse(AddClientResponse value) {
        return new JAXBElement<AddClientResponse>(_AddClientResponse_QNAME, AddClientResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDirectoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "createDirectoryResponse")
    public JAXBElement<CreateDirectoryResponse> createCreateDirectoryResponse(CreateDirectoryResponse value) {
        return new JAXBElement<CreateDirectoryResponse>(_CreateDirectoryResponse_QNAME, CreateDirectoryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HelloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "helloResponse")
    public JAXBElement<HelloResponse> createHelloResponse(HelloResponse value) {
        return new JAXBElement<HelloResponse>(_HelloResponse_QNAME, HelloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurPathResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "getCurPathResponse")
    public JAXBElement<GetCurPathResponse> createGetCurPathResponse(GetCurPathResponse value) {
        return new JAXBElement<GetCurPathResponse>(_GetCurPathResponse_QNAME, GetCurPathResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListDirResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "listDirResponse")
    public JAXBElement<ListDirResponse> createListDirResponse(ListDirResponse value) {
        return new JAXBElement<ListDirResponse>(_ListDirResponse_QNAME, ListDirResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileContent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "fileContent")
    public JAXBElement<FileContent> createFileContent(FileContent value) {
        return new JAXBElement<FileContent>(_FileContent_QNAME, FileContent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckDir }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "checkDir")
    public JAXBElement<CheckDir> createCheckDir(CheckDir value) {
        return new JAXBElement<CheckDir>(_CheckDir_QNAME, CheckDir.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MovFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "movFileResponse")
    public JAXBElement<MovFileResponse> createMovFileResponse(MovFileResponse value) {
        return new JAXBElement<MovFileResponse>(_MovFileResponse_QNAME, MovFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "changeFileResponse")
    public JAXBElement<ChangeFileResponse> createChangeFileResponse(ChangeFileResponse value) {
        return new JAXBElement<ChangeFileResponse>(_ChangeFileResponse_QNAME, ChangeFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Hello }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "hello")
    public JAXBElement<Hello> createHello(Hello value) {
        return new JAXBElement<Hello>(_Hello_QNAME, Hello.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "createFileResponse")
    public JAXBElement<CreateFileResponse> createCreateFileResponse(CreateFileResponse value) {
        return new JAXBElement<CreateFileResponse>(_CreateFileResponse_QNAME, CreateFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurPath }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "getCurPath")
    public JAXBElement<GetCurPath> createGetCurPath(GetCurPath value) {
        return new JAXBElement<GetCurPath>(_GetCurPath_QNAME, GetCurPath.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveDirResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "moveDirResponse")
    public JAXBElement<MoveDirResponse> createMoveDirResponse(MoveDirResponse value) {
        return new JAXBElement<MoveDirResponse>(_MoveDirResponse_QNAME, MoveDirResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckDirResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "checkDirResponse")
    public JAXBElement<CheckDirResponse> createCheckDirResponse(CheckDirResponse value) {
        return new JAXBElement<CheckDirResponse>(_CheckDirResponse_QNAME, CheckDirResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTreeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "getTreeResponse")
    public JAXBElement<GetTreeResponse> createGetTreeResponse(GetTreeResponse value) {
        return new JAXBElement<GetTreeResponse>(_GetTreeResponse_QNAME, GetTreeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MovFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "movFile")
    public JAXBElement<MovFile> createMovFile(MovFile value) {
        return new JAXBElement<MovFile>(_MovFile_QNAME, MovFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveDir }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "moveDir")
    public JAXBElement<MoveDir> createMoveDir(MoveDir value) {
        return new JAXBElement<MoveDir>(_MoveDir_QNAME, MoveDir.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDisk }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "createDisk")
    public JAXBElement<CreateDisk> createCreateDisk(CreateDisk value) {
        return new JAXBElement<CreateDisk>(_CreateDisk_QNAME, CreateDisk.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileProperties }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "fileProperties")
    public JAXBElement<FileProperties> createFileProperties(FileProperties value) {
        return new JAXBElement<FileProperties>(_FileProperties_QNAME, FileProperties.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "IOException")
    public JAXBElement<IOException> createIOException(IOException value) {
        return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FileSystem/", name = "changeFile")
    public JAXBElement<ChangeFile> createChangeFile(ChangeFile value) {
        return new JAXBElement<ChangeFile>(_ChangeFile_QNAME, ChangeFile.class, null, value);
    }

}
