/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2;

import FileSystemWS.FileSystemWS;
import FileSystemWS.FileSystemWS_Service;
import FileSystemWS.IOException_Exception;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author jostin
 */
public class Client2 {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException, IOException_Exception {
        FileSystemWS_Service service = new FileSystemWS_Service();
        final FileSystemWS client = service.getFileSystemWSPort();
        //System.out.println(client.hello("Juan"));\
        String client1 = "client2";
        System.out.println(client.addClient(client1));
        BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
        
        //ControllerClient controller = new ControllerClient();
        //User user1 = new User("josmarin","1234");
        //controller.addUser(user1);
        
        System.out.println("-----File System----");
        boolean exec = true;
        while(exec){
            System.out.print(client.getCurPath(client1) + ">");
            String command = buffer.readLine();
            String[] commands = command.split(" ");
            List<String> list = Arrays.asList(commands);
            String res;
            switch(commands[0]){
                case "crt":
                     res = client.createDisk(client1, list);
                     System.out.println(res);
                     break;
                case "fle":
                    if(client.checkDir(client1, commands[1])){
                        System.out.print("File already exists, replace this file? (y/n): ");
                        String answer = buffer.readLine();
                        if(answer.equals("n")){
                            break;
                        } 
                        res = client.removeFile(client1, list);
                    }  
                    String content = (String)command.subSequence(command.indexOf("\"")+1, command.lastIndexOf("\""));
                    client.createFile(client1, commands[1], commands[2], content);
                    break;
                case "mkdir":
                    if(client.checkDir(client1, commands[1])){
                        System.out.print("File already exists, replace this file? (y/n): ");
                        String answer = buffer.readLine();
                        if(answer.equals("n")){
                            break;
                        } 
                        res = client.removeFile(client1, list);
                    } 
                    res = client.createDirectory(client1, list);
                    System.out.println(res);
                    break;
                case "chdir":
                    if(!client.checkDir(client1, commands[1]) && !commands[1].equals("..")){
                        System.out.println("The system cannot find the path specified.\n");
                        break;
                    }
                    res = client.moveDir(client1, list);
                    System.out.println(res);
                    break;
                case "ldir":
                    res = client.listDir(client1);
                    System.out.println(res);
                    break;
                case "mfle":
                    if(!client.checkDir(client1, commands[1])){
                        System.out.println("Inexistence File\n");
                        break;
                    }
                    content = (String)command.subSequence(command.indexOf("\"")+1, command.lastIndexOf("\""));
                    client.changeFile(client1, commands[1], content);
                    break;
                case "ppt":
                    if(!client.checkDir(client1, commands[1])){
                        System.out.println("Inexistence File\n");
                        break;
                    }
                    res = client.fileProperties(client1, list);
                    System.out.println(res);
                    break;
                case "view":
                    if(!client.checkDir(client1, commands[1])){
                        System.out.println("Inexistence File\n");
                        break;
                    }
                    res = client.fileContent(client1, list);
                    System.out.println(res);
                    break;
                case "cpy":
                    res = client.copyFile(client1, list);
                    System.out.println(res);
                    break;
                case "mov":
                    res = client.movFile(client1, list);
                    System.out.println(res);
                case "rem":
                    if(!client.checkDir(client1, commands[1])){
                        System.out.println("Inexistence File\n");
                        break;
                    }
                    res = client.removeFile(client1, list);
                    System.out.println(res);
                    break;
                case "tree":
                    res = client.getTree(client1);
                    System.out.println(res);
                    break;
                default:
                   System.out.println("Invalid instruction\n");
            }
        }  
    }
    
}